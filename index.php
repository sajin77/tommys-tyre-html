<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
    <div class="home_page">
        <div class="order_box">
            <h2>
                order your <span>tyre</span> online ,
                Fit locally
            </h2>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Search Vehicle Reg</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Search by Tyre Size</button>
                </li>
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                    <div class="register_wrapper">
                        <div class="register_box">
                            <div class="blue_box">
                                <img src="assets/images/icons/circle.svg" alt="">
                                <h6>
                                    GB
                                </h6>
                            </div>
                            <div class="input_group">
                                <input type="text" placeholder="ENTER REG">
                            </div>
                        </div>
                        <ul class="select_radio">
                            <li>
                                <input type="radio" name="choose" id="">
                                <h6>
                                    Fully Fitted
                                </h6>
                            </li>
                            <li>
                                <input type="radio" name="choose" id="">
                                <h6>
                                    Mail Order
                                </h6>
                            </li>
                        </ul>
                        <button class="search_btn">
                            SEARCH BY REG
                        </button>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">
                    <div class="register_wrapper">
                        <!-- <div class="register_box">
                            <div class="blue_box">
                                <img src="assets/images/icons/circle.svg" alt="">
                                <h6>
                                    GB
                                </h6>
                            </div>
                            <div class="input_group">
                                <input type="text" placeholder="ENTER REG">
                            </div>
                        </div> -->
                        <div class="size_select">
                            <select name="" id="">
                                <option value="">
                                    Width
                                </option>
                                <option value="">
                                    select
                                </option>
                                <option value="">
                                    select
                                </option>
                            </select>
                            <select name="" id="">
                                <option value="">
                                    Profile
                                </option>
                                <option value="">
                                    profile
                                </option>
                                <option value="">
                                    Profile
                                </option>
                            </select>
                            <select name="" id="">
                                <option value="">
                                    Wheel Size
                                </option>
                                <option value="">
                                    Wheel Size
                                </option>
                                <option value="">
                                    select
                                </option>
                            </select>
                            <select name="" id="">
                                <option value="">
                                    Speed
                                </option>
                                <option value="">
                                    select
                                </option>
                                <option value="">
                                    select
                                </option>
                            </select>
                        </div>
                        <ul class="select_radio">
                            <li>
                                <input type="radio" name="choose" id="">
                                <h6>
                                    Fully Fitted
                                </h6>
                            </li>
                            <li>
                                <input type="radio" name="choose" id="">
                                <h6>
                                    Mail Order
                                </h6>
                            </li>
                        </ul>
                        <button class="search_btn">
                            SEARCH BY REG
                        </button>
                    </div>
                </div>
              </div>
        </div>
        <div class="swiper bannerSwiper">
            <div class="swiper-wrapper">
              
              <div class="swiper-slide">
                <div class="banner_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="banner_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                               
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="banner_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                               
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="banner_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                               
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="banner_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                              
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="banner_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="swiper-pagination"></div>
          </div>
       
        <div class="inear_banner">
            <div class="inear_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">

                            <div class="tyre_warehouselabel">
                                <img src="assets/images/TYREWAREHOUSE.png" alt="">
                            </div>

                            <ul class="fitting_centers">
                                <li>
                                    <div class="service_icon">
                                        <img src="assets/images/shop.png" alt="">
                                    </div>
                                    <h4>
                                        Choose Our fitting Hub
                                    </h4>
                                    <p>
                                        3000+ fitting Hubs Nationwide
                                    </p>
                                </li>
                                <li>
                                    <div class="service_icon">
                                        <img src="assets/images/icons/service.png" alt="">
                                    </div>
                                    <h4>
                                        Same-Day Fitting Available
                                    </h4>
                                    <p>
                                        Contact Us for Local Services
                                    </p>
                                </li>
                                <li>
                                    <div class="service_icon">
                                        <img src="assets/images/icons/pricing.svg" alt="">
                                    </div>
                                    <h4>
                                        Transparent Pricing
                                    </h4>
                                    <p>
                                        The Displayed Price Is Your Final Cost
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="step_section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="step_tyre">
                            <img src="assets/images/icons/step_tyre.png" alt="">
                        </div>
                        <h2>
                            <img src="assets/images/icons/elipse_head.svg" alt="">
                            Secure your <span>Perfect Tyres </span>
online in 4 simple steps
                        </h2>
                        <ul>

                            <div class="line">
                                <svg width="1061" height="211" viewBox="0 0 1061 211" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.23287 108.342C62.3796 181.763 337.03 294.525 345.595 90.5887C376.109 6.20881 554.409 -80.1508 723.803 140.558C746.389 169.986 1001.68 305.986 1059.97 90.5887" stroke="black" stroke-dasharray="15 15"/>
                                    </svg>
                            </div>

                            <li class="active">
                                <div class="count">
                                    1
                                </div>
                                <h3>
                                    Select Your Tyres
                                </h3>
                                <p>
                                    Browse and pick the best tyres 
tailored for your vehicle's needs.
                                </p>
                            </li>
                            <li>
                                <div class="count">
                                    2
                                </div>
                                <h3>
                                    Schedule Fitter & Time
                                </h3>
                                <p>
                                    Set up a convenient appointment 
with a professional tyre fitter
                                </p>
                            </li>
                            <li>
                                <div class="count">
                                    3
                                </div>
                                <h3>
                                    Swift Payment Online
                                </h3>
                                <p>
                                    Effortlessly complete your purchase with our fast and secure online payment
                                </p>
                            </li>
                            <li>
                                <div class="count">
                                    4
                                </div>
                                <h3>
                                    Secure & Go
                                </h3>
                                <p>
                                    Hit the road with confidence on your newly fitted tyres.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

 
        <section class="tyre_Listing">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                       <div class="listing_head">
                        <h2>
                            <img src="assets/images/icons/elipse_head.svg" alt="">
                            OUR POPULAR CAR <span>TYRE</span> RANGES
                        </h2>
                        <a href="">See All</a>
                       </div>

                       <ul class="listing_ul">
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                        <li><a href="">
                            <div class="product_img1">
                                <img src="assets/images/tyre.png" alt="">
                            </div>
                            <ul class="specs">
                                <li class="fuel">
                                    <img src="assets/images/icons/Fuel.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="rain">
                                    <img src="assets/images/icons/Rain.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                                <li class="Speaker">
                                    <img src="assets/images/icons/Speaker.svg" alt="">
                                    <h6>
                                        E
                                    </h6>
                                </li>
                            </ul>
                            <h3>
                                Continental Sport Contact
                            </h3>
                            <h4>
                                £280.3
                            </h4>

                           

                           
                            <button type="button" class="add_cart">
                                <img src="assets/images/icons/cart_white.svg" alt="">
                                Add to Cart
                            </button>

                        </a></li>
                    </ul>

                    </div>
                </div>
            </div>
        </section>


        <div class="tyrebrand_section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>
                            <img src="assets/images/icons/elipse_head.svg" alt="">
                            OUR POPULAR CAR <span>TYRE</span> RANGES
                        </h2>

                       
                        <div class="brand_swiperwrapper">
                            <div class="brands_backgrnd">
                                <img src="assets/images/brands_bg.png" alt="">
                            </div>
    
                            <div class="swiper brands_slider">
                                <div class="swiper-wrapper">
                                  <div class="swiper-slide">
                                    <div class="brand_img">
                                        <img src="assets/images/brand1.png" alt="">
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                     <div class="brand_img">
                                        <img src="assets/images/brand2.png" alt="">
                                     </div>
                                  </div>
                                  <div class="swiper-slide">
                                    <div class="brand_img">
                                        <img src="assets/images/brand3.png" alt="">
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                    <div class="brand_img">
                                        <img src="assets/images/brand3.png" alt="">
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                     <div class="brand_img">
                                        <img src="assets/images/brand3.png" alt="">
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                    <div class="brand_img">
                                        <img src="assets/images/brand3.png" alt="">
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                      <div class="brand_img">
                                        <img src="assets/images/brand3.png" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="swiper-pagination" id="brand_dots"></div>
                              </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="tyre_typesection">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="tyretype_banner">
                            <img src="assets/images/tyre_type.png" alt="">
                        </div>
                        <h2>
                            <img src="assets/images/icons/elipse_head.svg" alt="">
                            ULTIMATE <span>TYRE</span> SELECTION
                        </h2>
                        <h3>
                            <img src="assets/images/icons/elipse_head.svg" alt="">
                            NAVIGATE ALL <span>ROADS</span>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="type_ul">
                            <li>
                                <div class="type_image">
                                    <img src="assets/images/type1.png" alt="">
                                </div>
                                <div class="type_box">
                                    RUNFLAT TYRES
                                </div>
                            </li>
                            <li>
                                <div class="type_image">
                                    <img src="assets/images/type2.png" alt="">
                                </div>
                                <div class="type_box">
                                    ALL SEASON TYRES
                                </div>
                            </li>
                            <li>
                                <div class="type_image">
                                    <img src="assets/images/type3.png" alt="">
                                </div>
                                <div class="type_box">
                                    BUDGET CAR TYRES
                                </div>
                            </li>
                            <li>
                                <div class="type_image">
                                    <img src="assets/images/type4.png" alt="">
                                </div>
                                <div class="type_box">
                                    HYBRID AND ELECTRIC TYRES
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        


        <section class="faq_section">

            <div class="faq_elipse1">
                <img src="assets/images/faq_elipse1.png" alt="">
            </div>
            <div class="faq_elipse2">
                <img src="assets/images/faq_elipse2.png" alt="">
            </div>
            <div class="faq_tyre">
                <img src="assets/images/faq_tyre.png" alt="">
            </div>
           
            <div class="faq_banner">

                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="faq_head">
                                <img src="assets/images/icons/elipse_head.svg" alt="">
                                FREQUENTLY ASKED <span>QUESTIONS</span>
                            </h2>
                            <div class="faq_accordionWrapper">

                                

                                <div class="accordion" id="accordionExample">
                                    <div class="accordion-item">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <div class="accorrdion_head">
                                                Can I put bigger size tyres on my car?
                                            </div>
                                          
                                        </button>
                                      </h2>
                                      <div id="collapseOne" class="accordion-collapse collapse show" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            Our Maximum Value Tyres represent the perfect balance between cost and quality. Designed for the savvy buyer, these tyres offer enhanced durability and performance at a fraction of the price of premium brands. Ideal for those seeking great performance and longevity without the hefty price tag.
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                           
                                            <div class="accorrdion_head">
                                                What happens if we increase tyre size?
                                            </div>
                                         
                                           
                                            
                                        </button>
                                      </h2>
                                      <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            Our Maximum Value Tyres represent the perfect balance between cost and quality. Designed for the savvy buyer, these tyres offer enhanced durability and performance at a fraction of the price of premium brands. Ideal for those seeking great performance and longevity without the hefty price tag.
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            
                                            <div class="accorrdion_head">
                                                Do large tyres affect mileage?
                                            </div>
                                       

                                           
                                        </button>
                                      </h2>
                                      <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            Our Maximum Value Tyres represent the perfect balance between cost and quality. Designed for the savvy buyer, these tyres offer enhanced durability and performance at a fraction of the price of premium brands. Ideal for those seeking great performance and longevity without the hefty price tag.
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header">
                                          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefour" aria-expanded="false" aria-controls="collapseThree">
                                            
                                            <div class="accorrdion_head">
                                                Which tyres give the most mileage?
                                            </div>
                                            
                                          </button>
                                        </h2>
                                        <div id="collapsefour" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                          <div class="accordion-body">
                                            Our Maximum Value Tyres represent the perfect balance between cost and quality. Designed for the savvy buyer, these tyres offer enhanced durability and performance at a fraction of the price of premium brands. Ideal for those seeking great performance and longevity without the hefty price tag.
                                          </div>
                                        </div>
                                      </div>
                                  </div>


                                <div class="seeall_btn">
                                    <a href="">See All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           
        </section> 
   
    </div>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
