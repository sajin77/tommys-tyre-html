<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="menubar">
                        <div class="logo_nav">
                            <div class="logo">
                                <img src="assets/images/icons/logo.svg" alt="">
                            </div>
                            <div class="navbar">
                                <ul>
                                    <li><a href="">
                                        Home
                                    </a></li>
                                    <li><a href="">
                                        About us
                                    </a></li>
                                    <li><a href="">
                                        Tyres <img src="assets/images/icons/arrowdown.svg" alt="">
                                    </a></li>
                                    <li><a href="">

                                    </a></li>
                                    <li><a href="">
                                        Offers
                                    </a></li>
                                    <li><a href="">
                                        Contact Us
                                    </a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="contact_details">
                            <ul>
                                <li>
                                    <button> <img src="assets/images/icons/phone.svg" alt=""> 0800 652 3120</button>
                                </li>
                                <li>
                                    <a href="">
                                        <img src="assets/images/icons/profile.svg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <img src="assets/images/icons/cart.svg" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</header>




  
  <!-- Modal -->
  <div class="modal chekout_pop fade" id="confirmation_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <div class="confirmation_image">
            <img class="check_img" src="assets/images/confirm_check.png" alt="">

            <div class="particle_img">
                <img src="assets/images/checkout_particle.png" alt="">
            </div>
          </div>
        </div>
        <div class="modal-body">
          <h2>
            Order Confirmed
          </h2>
          <h5>
            Order ID: #234345
          </h5>
          <h4>
            Thankyou for placing your order with M8 Tyres.
          </h4>
          <div class="chekout_itemwrapper modal_checkwrapper" >
            <div class="product_head">
                <h6>
                    Product Details
                </h6>
                <h6>
                    price
                </h6>
            </div>
            <div class="cart_item">
                <div class="item_details">
                    <div class="cart_img">
                        <img src="assets/images/cart_item.png" alt="">
                    </div>
                    <div class="item_name">
                        <h3>
                            Uni Royal Rain Sport
                        </h3>
                        <ul class="specs">
                            <li class="fuel">
                                <img src="assets/images/icons/Fuel.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                            <li class="rain">
                                <img src="assets/images/icons/Rain.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                            <li class="Speaker">
                                <img src="assets/images/icons/Speaker.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="quantity_wrapper">
                    <div class="price">
                        <h5>
                            £104.52
                        </h5>
                    </div>
                </div>
            </div>
            <div class="cart_item">
                <div class="item_details">
                    <div class="cart_img">
                        <img src="assets/images/cart_item.png" alt="">
                    </div>
                    <div class="item_name">
                        <h3>
                            Uni Royal Rain Sport
                        </h3>
                        <ul class="specs">
                            <li class="fuel">
                                <img src="assets/images/icons/Fuel.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                            <li class="rain">
                                <img src="assets/images/icons/Rain.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                            <li class="Speaker">
                                <img src="assets/images/icons/Speaker.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="quantity_wrapper">
                    <div class="price">
                        <h5>
                            £104.52
                        </h5>
                    </div>
                </div>
            </div>
            <div class="cart_item">
                <div class="item_details">
                    <div class="cart_img">
                        <img src="assets/images/cart_item.png" alt="">
                    </div>
                    <div class="item_name">
                        <h3>
                            Uni Royal Rain Sport
                        </h3>
                        <ul class="specs">
                            <li class="fuel">
                                <img src="assets/images/icons/Fuel.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                            <li class="rain">
                                <img src="assets/images/icons/Rain.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                            <li class="Speaker">
                                <img src="assets/images/icons/Speaker.svg" alt="">
                                <h6>
                                    E
                                </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="quantity_wrapper">
                    <div class="price">
                        <h5>
                            £104.52
                        </h5>
                    </div>
                </div>
            </div>
            <div class="grand_total">
                <h6>
                    Total
                </h6>
                <h5>
                    £320.52
                </h5>
            </div>
           </div>

           <div class="continue_btn">
            <button>
                Continue Shopping
            </button>
           </div>
        </div>
      </div>
    </div>
  </div>