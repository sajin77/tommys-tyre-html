<footer>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="main_footer">
                        <div class="logo">
                            <a href="">
                                <img src="assets/images/icons/logo.svg" alt="">
                            </a>
                        </div>
                        <div class="quick_links_newsletter">
                            <div class="map_quicklinks">
                                <div class="map">
                                    <img src="assets/images/map.png" alt="">
                                </div>
                                <div class="quick_links">
                                    <h3>
                                        Quick Links
                                    </h3>
                                    <ul>
                                        <li><a href="">
                                            Home
                                        </a></li>
                                        <li><a href="">
                                            About Us
                                        </a></li>
                                        <li><a href="">
                                            Tyres 
                                                
                                        </a><svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.5 10L12.5 15L17.5 10H7.5Z" fill="white"/>
                                            </svg></li>
                                        <li><a href="">
                                            Brands 
                                                
                                        </a><svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.5 10L12.5 15L17.5 10H7.5Z" fill="white"/>
                                            </svg></li>
                                        <li><a href="">
                                            Offers
                                        </a></li>
                                        <li><a href="">
                                            Contact Us
                                        </a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news_letter">
                                <h4>
                                    Get Latest Updates and Offers
                                </h4>
                                <div class="input_wrapper">
                                    <input type="text" placeholder="Your e mail address">
                                    <button>Subscribe</button>
                                </div>
                            </div>
                        </div>
                        <div class="contact_details">
                            <ul>
                                <li>
                                    <img src="assets/images/icons/location.svg" alt="">
                                    <label style="max-width: 13.6rem; width: 100%;" for="">2963, Road Manchester UK</label>
                                </li>
                                <li>
                                       <div class="contact_img">
                                        <img src="assets/images/icons/email.svg" alt="">
                                       </div>
                                       <div class="mail">
                                        <label for="">E-Mail Us</label>
                                        <a href="">info@gmail.com</a>
                                       </div>
                                </li>
                                <li>
                                    <div class="contact_img">
                                     <img src="assets/images/icons/call.svg" alt="">
                                    </div>
                                    <div class="mail">
                                     <label for="">Call For Support</label>
                                     <a href="">0800 652 3120</a>
                                    </div>
                             </li>
                             <li>
                                <div class="contact_img">
                                 <img src="assets/images/icons/global.svg" alt="">
                                </div>
                                <div class="mail">
                                 <label for="">Follow Us</label>
                                 <div class="social_links">
                                    <a href=""><img src="assets/images/icons/twitter.svg" alt=""></a>
                                    <a href=""><img src="assets/images/icons/facebook.svg" alt=""></a>
                                 </div>
                                </div>
                         </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sub_footer">
                        <div class="copy_right">
                            © 2024 Tyrewarehouse. All Rights Reserved
                        </div>
                        <ul>
                            <li><a href="">
                                Privacy Policy
                            </a></li>
                            <li><a href="">
                                Terms & Conditions
                            </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</footer>