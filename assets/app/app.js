// const header = document.querySelector(".header");
// const toggleClass = "is-sticky";

// window.addEventListener("scroll", () => {
//   const currentScroll = window.pageYOffset;
//   if (currentScroll > 150) {
//     header.classList.add(toggleClass);
//   } else {
//     header.classList.remove(toggleClass);
//   }
// });

  // niceselect js

  $("select").niceSelect();

let toggle_btn = document.querySelector('.toggle');
let mobile_menu = document.querySelector('.menu_bar');
let body = document.querySelector('body');

function mobileMenu() {
    mobile_menu.classList.toggle('active');
    body.classList.toggle('overflow_hidden');
}

var bannerSwiper = new Swiper(".bannerSwiper", {
  slidesPerView: 1,
  speed:1100,



    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },

});

var brands_slider = new Swiper(".brands_slider", {
    slidesPerView: 5,
    spaceBetween: 54,
    speed:1100,

    breakpoints: {
        320: {
            slidesPerView: 2.5,
            spaceBetween: 20,
          },
          568: {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        768: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
        991: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
        1200: {
          slidesPerView: 5,
          spaceBetween: 54,
        },
      },

      pagination: {
        el: "#brand_dots",
        clickable: true,
      },

      // autoplay: {
      //   delay: 2500,
      //   disableOnInteraction: false,
      // },

});